Please perform following steps in order to test this (accurate for Ubuntu 20.04):
---------------------------------------------------------------------------------

(in terminal:)

	$ git clone https://bitbucket.org/fifajan/filestor.git
	$ pip3 install -r requirements.txt
	$ cd filestore
	$ python3 manage.py migrate
	$ python3 manage.py runserver localhost:4321

(in separate terminal:)

	$ cd filestore
	$ python3 manage.py sync_images
	$ python3 test.py
