from django.core.management.base import BaseCommand, CommandError
from rest_framework.status import HTTP_200_OK

from images.models import Image

from requests import get, post


API_KEY = '23567b218376f79d9415'
BASE_URL = 'http://interview.agileengine.com/'
AUTH_URL = BASE_URL + 'auth'
IMAGES_URL = BASE_URL + 'images'


class Command(BaseCommand):
    help = 'Syncs image database with remote server'

    def handle(self, *args, **options):
        sync_images()


def sync_images():
    headers = {'Authorization': 'Bearer ' + get_token()}
    get_image_list(headers)


def get_token():
    response = post(AUTH_URL, json={'apiKey': API_KEY})

    if response.status_code != HTTP_200_OK:
        raise Exception('Cannot auth on remote server!')

    return response.json()['token']


def get_image_list(headers):
    response = get(IMAGES_URL, headers=headers)

    if response.status_code != HTTP_200_OK:
        raise Exception('Cannot fetch image list from remote server!')

    page_range = range(1, response.json()['pageCount'] + 1)
    for page in page_range:
        if page > 1:
            response = get(IMAGES_URL + '?page={}'.format(page),
                           headers=headers)
            if response.status_code != HTTP_200_OK:
                raise Exception(
                    'Cannot fetch image list from remote server!')

        for picture in response.json()['pictures']:
            picture_info = get_image_details(headers, picture['id'])
            _, created = Image.objects.get_or_create(
                id=picture_info['id'],
                defaults={'author': picture_info['author'],
                          'camera': picture_info.get('camera'),
                          'tags': picture_info['tags'],
                          'cropped_url': picture_info['cropped_picture'],
                          'full_url': picture_info['full_picture'],
                }
            )
            if created:
                print('Created: {}'.format(picture_info['id']))


def get_image_details(headers, image_id):
    response = get(IMAGES_URL + '/' + image_id, headers=headers)

    if response.status_code != HTTP_200_OK:
        raise Exception('Cannot fetch details for image: {}'.format(image_id))

    return response.json()
