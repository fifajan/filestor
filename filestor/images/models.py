from django.db import models


class Image(models.Model):
    id = models.CharField(max_length=20, primary_key=True)
    author = models.CharField(max_length=100)
    camera = models.CharField(max_length=100, null=True)
    tags = models.CharField(max_length=300)
    cropped_url = models.URLField(max_length=200)
    full_url = models.URLField(max_length=200)
