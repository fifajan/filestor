from images.models import Image
from rest_framework import serializers


class ImageDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ['id', 'author', 'camera', 'tags',
                  'cropped_url', 'full_url']


class ImageListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ['id', 'cropped_url']
