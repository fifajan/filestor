from django.urls import include, path
from django.conf.urls import url
from rest_framework import routers
from images import views


router = routers.DefaultRouter()
router.register(r'images', views.ImageViewSet)


urlpatterns = [
    path('', include(router.urls)),
    url('^search/(?P<substring>.+)/$', views.ImageSearch.as_view()),
]

