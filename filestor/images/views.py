from django.shortcuts import render
from django.db.models import Q


from images.models import Image
from rest_framework import viewsets, generics
# from rest_framework import permissions
from images.serializers import ImageDetailSerializer, ImageListSerializer


class ImageViewSet(viewsets.ModelViewSet):
    queryset = Image.objects.all()
    # permission_classes = [permissions.IsAuthenticated]

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return ImageDetailSerializer
        return ImageListSerializer


class ImageSearch(generics.ListAPIView):
    serializer_class = ImageListSerializer

    def get_queryset(self):
        substring = self.kwargs['substring']

        return Image.objects.filter(
            Q(author__icontains=substring) | Q(
                camera__icontains=substring) | Q(tags__icontains=substring))    
