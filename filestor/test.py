from requests import get, post
import pprint


URL = 'http://localhost:4321'
pp = pprint.PrettyPrinter(indent=2).pprint

print('\nFetching paginated photo feed in JSON format:\n----')

resp = get(URL + '/images')
pp(resp.json())

print('\nFetching specific page photo feed:\n----')

resp = get(URL + '/images/?page=4')
pp(resp.json())

print('\nFetching specific photo details by id:\n----')

resp = get(URL + '/images/84b0995b78a9e54c9c0c')
pp(resp.json())

print('\nSearching photo by substring in attributes:\n----')

resp = get(URL + '/search/photo')
pp(resp.json())
